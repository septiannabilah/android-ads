package com.example.gt303_12.breakoutgame;

import android.app.Activity;
//-import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


import java.io.IOException;

public class BreakoutView extends Activity {

    BreakoutGame breakoutGame;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        breakoutGame = new BreakoutGame(this);

        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        FrameLayout game = new FrameLayout(this);

        Button endGameButton = new Button(this);

        endGameButton.setWidth(300);
        endGameButton.setText("Start Game");

        //Banner
        AdView adView = new AdView(this);
        adView.setAdSize(AdSize.BANNER);
        adView.setAdUnitId("ca-app-pub-3940256099942544/6300978111");

        RelativeLayout relativeLayout = new RelativeLayout(this);

        RelativeLayout.LayoutParams adViewParams = new RelativeLayout.LayoutParams(
                AdView.LayoutParams.WRAP_CONTENT,
                AdView.LayoutParams.WRAP_CONTENT);
        // align bottom
        adViewParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        //adView.setAdSize(AdSize.BANNER, 500, 10);
        // align center
        adViewParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

        relativeLayout.addView(adView, adViewParams);


        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        game.addView(breakoutGame);
        game.addView(relativeLayout);

        setContentView(game);
    }

    @Override
    protected void onResume() {
        super.onResume();
        breakoutGame.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        breakoutGame.pause();
    }
}